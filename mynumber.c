#include <stdio.h>

int main()
{
	int n = 1;
	while (n < 101)
	{
		if (n%2 == 1)
			printf("%d ", n);		
		n++;
	}
	printf("\n");

	int i;
	for (i = 1; i < 101; i++)
	{
		if (i%2 == 0)
			printf("%d ", i);
	}
	printf("\n");

	for (n = 2; n <= 100; n++)
	{
		for (i = 2; i < n; i++)
		{
			if (n % i == 0) break;
		}
		if (n == i) printf("%d ", n);
	}
	printf ("\n");

	return 0;
}
